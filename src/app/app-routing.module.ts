import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import { MovieComponent } from "./movie/movie.component";
import { MovieDetailsComponent } from './movie/movie-details/movie-details.component';
import { UserComponent } from './user/user.component';
import { HomeComponent } from './home/home.component';
import { SignUpComponent } from './user/sign-up/sign-up.component';
import { SignInComponent } from './user/sign-in/sign-in.component';
import { AuthGuard } from './auth/auth.guard';
import { AddMovieComponent } from './movie/add-movie/add-movie.component';
import { UpdateMovieComponent } from './movie/update-movie/update-movie.component';



const routes: Routes = [

  { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },       // parent component with children
                                                                              // treb sa fii logat sa ajungi la home
      {
        path: 'signup', component: UserComponent,
        children: [{ path: '', component: SignUpComponent }]
      },
      {
        path: 'login', component: UserComponent,
        children: [{ path: '', component: SignInComponent }]
      },

  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'fetch-movies', component: MovieComponent },
  { path: 'fetch-movies/:id', component: MovieDetailsComponent },
  { path: 'add', component: AddMovieComponent },
  { path: 'update/:id', component: UpdateMovieComponent },
  { path: 'delete/:id', component: MovieComponent },
  { path: '**', component: HomeComponent } // in caz de pageNotFound
 
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
