import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { ToastrModule } from 'ngx-toastr';


import { SocialLoginModule, AuthServiceConfig } from 'angularx-social-login';
import { FacebookLoginProvider } from 'angularx-social-login';

let config = new AuthServiceConfig([
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider("384423225851060") //FB appID
  },
]);
export function provideConfig() {
  return config;
}


import { AppComponent } from './app.component';

import { MovieListComponent } from './movie/movie-list/movie-list.component';
import { MovieService } from './movie/shared/movie.service';
import { MovieDetailsComponent } from './movie/movie-details/movie-details.component';
import { UserService } from './user/shared/user.service';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { SignUpComponent } from './user/sign-up/sign-up.component';
import { SignInComponent } from './user/sign-in/sign-in.component';
import { UsersListComponent } from './user/users-list/users-list.component';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { AuthGuard } from './auth/auth.guard';
import { AuthInterceptor } from './auth/auth.interceptor';
import { AddMovieComponent } from './movie/add-movie/add-movie.component';
import { UpdateMovieComponent } from './movie/update-movie/update-movie.component';
import { MovieComponent } from './movie/movie.component';


@NgModule({

  declarations: [
    AppComponent,
    NavMenuComponent,
    MovieComponent,
    MovieListComponent,
    MovieDetailsComponent,
    AddMovieComponent,
    UpdateMovieComponent,
    SignUpComponent,
    SignInComponent,
    HomeComponent,
    UserComponent,
    UsersListComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    ReactiveFormsModule,
    AppRoutingModule,
    SocialLoginModule.initialize(config)
  ],

  providers: [MovieService, UserService, AuthGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
     {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    }
  ],

  bootstrap: [AppComponent]
})

export class AppModule { }
