import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from './user.model';
import { map} from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { AuthService } from 'angularx-social-login';
import { FBUser } from './FBUser.model';




@Injectable({
    providedIn: 'root'
})



  // https://jasonwatmore.com/post/2019/06/22/angular-8-jwt-authentication-example-tutorial


export class UserService {

    readonly rootURL = "https://localhost:44311";
    formDataRegister: User;

  public currentUserSubject: BehaviorSubject<any>;    // fie localUser fie FBUser
    public currentUser: Observable<any>;



  constructor(private http: HttpClient,
              private authService : AuthService) {  // socialUser pt Logout

    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();

  }







    postRegister() {
        console.log(this.formDataRegister);
        return this.http.post(this.rootURL + '/users/register', this.formDataRegister);   // POST function returns Observable
   
    }







  public get currentUserValue(): any {  // fie localUser fie FBUser
    return this.currentUserSubject.value;
  }







  // Along with userName and password you have to pass grant_type as password.
  // With HttpHeader, we have set Content-Type as application / x - www - urlencoded.Additional No - Auth property is set to True

  userAuthentication(username, password) {
    var data = { username: username, password: password };

    console.log(data);

    return this.http.post<User>(this.rootURL + '/users/authenticate', data)
      .pipe(map(user => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
      
        return user;
      }));
    
  }






  FBAuthentication(userdata) {
    return this.http.post<FBUser>(this.rootURL + '/users/authenticate-facebook', userdata).pipe(
      map(user => {
        //if (user.message != null) {
        //  console.log('We sent a message to our Controller API. It works');

          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject.next(user);
          console.log("merge");
          console.log(user);
          return user;
        
       
      })
    );
  }







  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);

    // social user
    this.authService.signOut();
  
  }
 


   
               
            

  getUsers() {
    return this.http.get<User[]>(this.rootURL + '/users');       //, {
    //    headers: new HttpHeaders()
    //        .set('Authorization', this.currentUserValue.Token)
    //});
  } 
}
