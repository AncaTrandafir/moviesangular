import { Component, Inject, OnInit } from '@angular/core';
import { MovieService } from '../shared/movie.service';
import { Movie } from '../shared/movie.model';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styles: []
})

export class MovieListComponent implements OnInit {

  public movieList: Movie[];
  public filteredMovies: Movie[];
  errorMessage: string;
  submitPressed = false;



  constructor(private service: MovieService,
    private toastr: ToastrService) { }



  ngOnInit() {  // populam lista
    this.getAllMovies();
    console.log(this.movieList);

    //this.service.getMovies()
    //  .subscribe(
    //    (data: any[]) => {
    //      console.log(data);
    //      this.movieList = data
    //    },
    //    error => {
    //      console.log(error);
    //    });

    //console.log(this.movieList);
  }




    // in acelasi tabel afisez fie toate filmele, fie pe cele filtrate
    // diferenta o face o variabila submitPressed care initial este false si isi schimba valoarea daca se apasa submit

  onSubmit(form: NgForm) {
    this.submitPressed = true; // inseamna  ca s-a accesat input-ul de filter si returnam filteredMovies
    this.getFilteredMoviesByDate(form);

  }




  getAllMovies() {
    this.service.getMovies()
       //.subscribe(data => {
       //    console.log(data);
       //  this.movieList = data;
       //  console.log(this.movieList);
       //  },
       //  error => {
       //    console.log(error);
       //  });
      .toPromise()
      .then(response => {
        console.log(response);
        this.movieList = response as Movie[]
      },
         error => {
           console.log(error);
         });

  }
  



  getFilteredMoviesByDate(form: NgForm) {

    console.log(form.value.from);

    this.service.filterMoviesByDate(form.value.from, form.value.to)     //form.value.
      // .subscribe(data => {
      .toPromise()
      .then(response => {
        this.movieList = response;
        console.log(this.movieList)
      });
  }





  populateForm(movie: Movie)  // populam form cu movie selectat si incarcat din server
  {                           // form data este proprietatea din service referitoare la form

   // this.service.formData = movie;  // se editeaza live in lista si nu vreau, si atunci creez copie cu object.assign

    this.service.formDataMovie = Object.assign({}, movie);  // in formData pune o copie a obiectului meu movie
   
    // Since the form is bound to formData properties,
    // the form field will get with populated corresponding details.

    console.log("click works");
    console.log(movie);
    console.log(movie.Title);
    console.log(this.service.formDataMovie.Title);
  }





  onDelete(id) {
    console.log("id este " + id);
    if (confirm("Are you sure you want to delete this record?")) {

      this.service.deleteMovie(id)
        .subscribe(response => {
          this.service.getMovies(); 
          this.toastr.warning('Deleted successfully', 'Movies');   // Att: apostroafe, nu ghilimele
        },
          error => {
            console.log(error);
          });

    }
  }
}
