export class Movie {
  Id: number;
  Title: string;
  Description: string;
  Genre: Genre;
  Duration: number;
  YearOfRelease: number;
  Director: string;
  DateAdded: string;
  Rating: number;
  Watched: boolean;
}


export enum Genre {
  Adventure,
  Comedy,
  Horror,
  SciFi
}
