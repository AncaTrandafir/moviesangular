import { Injectable } from '@angular/core';
import { Movie } from './movie.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Observable, of } from "rxjs";
import { catchError, map, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class MovieService {

  readonly rootURL = "https://localhost:44311";
  public formDataMovie: Movie;
  public selectedMovie: Movie;
  public updateBtnMovieClicked = false;  // initial butonul de update nu e apasat; il importam in fetch-data
  public idCopied: number;  // copiez id sa pot apela la update pt ca voi redefini id ca undefined intre timp
  movieList: Movie[];


  constructor(private http: HttpClient,
              public toastr: ToastrService ) {   // injectam HttpClient, to consume API
                                                  // injectam toastr in constructor
  }




  postMovie() {    
    return this.http.post(this.rootURL + '/movies', this.formDataMovie);   // POST function returns Observable
    // returneaza un movie tip service.formData
  }





  getMovies() {
    return this.http.get<Movie[]>(this.rootURL + '/movies');
      // .subscribe(                     // tratez in service subscribe de Observable pt ca o voi tot apela ca refresh si sa injectez doar service, nu si alte componente
      //(data: any[]) => {
      //  console.log(data);
      //  this.movieList = data
      //}); // Nu imi afiseaza cu subscribe in service, dar daca pun subscribe in movie-list merge

      // Since the sendGetRequest() method returns the return value of the HttpClient.get() method which is an RxJS Observable,
      // we subscribed to the returned Observable to actually send the HTTP GET request and process the HTTP response.
      // When data is received, we added it in the products array.

      //.toPromise()
      //.then(response => {
      //  console.log(response);
      //  this.movieList = response as Movie[]
      //});
  }







  updateMovie(formData: Movie) {
    return this.http.put(this.rootURL + '/movies/' + formData.Id, this.formDataMovie);   // transmite URL si ID in PUT request prin concatenare
    // returneaza un Observable
  }





  deleteMovie(id) {
    return this.http.delete(this.rootURL + '/movies/' + id);  
    // returneaza un Observable
  }




  // GET: movies/filter?from=a&to=b
  filterMoviesByDate(from, to) {
    return this.http.get<Movie[]>(this.rootURL + '/movies/filter?from=' + from + '&to=' + to);
  }

}
